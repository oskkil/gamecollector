package edu.put.inf151863

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import java.io.File

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val startButton = findViewById<Button>(R.id.startButton)
        val usernameField = findViewById<EditText>(R.id.username)
        var username = ""
        val loadLastUserButton = findViewById<Button>(R.id.loadLastUserButton)
        val lastSynchTextView = findViewById<TextView>(R.id.lastSynch)
        var multipleUser = ""
        var current = ""
        var usernametmp = ""
        fun find_username(){
            //val xmlDirectory = File("$filesDir/XML")
            val xmlDirectory = File("/data/user/0/edu.put.inf151863/files/XML")
            if (!xmlDirectory.exists()) xmlDirectory.mkdir()
            //File("$filesDir/XML/").walk().forEach {
            var numberOfUsersFound = 0
            File("/data/user/0/edu.put.inf151863/files/XML/").walk().forEach {
                val fileName = it.toString()
                Log.d("Existing file: ", fileName)
                val list = fileName.split("/")
                for (one in list){
                    if (one.contains("info")){
                        val userlist = one.split("_")
                        username = userlist[0]
                        if (username != usernametmp) {
                            numberOfUsersFound += 1
                            usernametmp = username
                        }
                        current = userlist[1]
                        Log.d("Found username: ", username)
                        Log.d("Last synchronization: ", current)
                    }
                    else continue
                }
            }
            Log.d("Found x number of users: ", numberOfUsersFound.toString())
            if (username != "") {
                loadLastUserButton.text = "Last user: $username"
            }
            else {
                loadLastUserButton.text = "No user found"
            }
            if (numberOfUsersFound > 1){
                loadLastUserButton.text = "Multiple users found! Click me to delete database and try again."
                multipleUser = "multiple"
            }
            if (current != ""){
                lastSynchTextView.text = "Last synchronization for user:\n$username\n$current"
            }
            else {
                lastSynchTextView.text = "No user in database!\nNo last synchronization!"
            }
        }
        loadLastUserButton.setOnClickListener{
            if (loadLastUserButton.text != "No user found"){
                val userlistURL = "https://www.boardgamegeek.com/xmlapi2/collection?username=$username&stats=1"
                val intent = Intent(this, UserPanel::class.java)
                intent.putExtra("Username", username)
                intent.putExtra("UserlistURL", userlistURL)
                intent.putExtra("MultipleUser", multipleUser)
                startActivity(intent)
            }
        }
        fun start(){
            if (usernameField.text.isNotEmpty()){
                try {
                    username = usernameField.text.toString()
                    val userlistURL = "https://www.boardgamegeek.com/xmlapi2/collection?username=$username&stats=1"
                    //Toast.makeText(this, userlistURL, Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, UserPanel::class.java)
                    intent.putExtra("Username", username)
                    intent.putExtra("UserlistURL", userlistURL)
                    intent.putExtra("MultipleUser", multipleUser)
                    startActivity(intent)

                } catch (e: Exception) {
                    Toast.makeText(this, "Invalid username!",Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this,"Empty username!",Toast.LENGTH_SHORT).show()
            }
        }
        startButton.setOnClickListener {
            start()
        }
        find_username()
    }

}