package edu.put.inf151863

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isEmpty
import androidx.core.view.marginBottom
import androidx.core.view.setMargins
import kotlinx.coroutines.*
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import java.io.*
import java.net.MalformedURLException
import java.net.URL
import java.nio.charset.Charset
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.Executors
import javax.xml.parsers.DocumentBuilderFactory
import kotlin.system.exitProcess


class UserPanel : AppCompatActivity() {
    class Item {
        var name: String? = null
        var original_name: String? = null
        var year: Int = 0
        var id: Long = 0
        var photo: String? = null
        var extension: Int = 0

        constructor(name: String, original_name: String, year: Int, id: Long, photo: String?, extension: Int) {
            this.name = name
            this.original_name = original_name
            this.year = year
            this.id = id
            this.photo = photo
            this.extension = extension
        }
    }
    class MyDBHandler(
        context: Context,
        factory: SQLiteDatabase.CursorFactory?,
    ) : SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {
        companion object {
            private val DATABASE_VERSION = 1
            private val DATABASE_NAME = "itemDB.db"
            val BASE = "DataBase"
            val NAME = "name"
            val ORIGINAL_NAME = "original_name"
            val YEAR = "year"
            val ID = "id"
            val PHOTO = "photo"
            val EXTENSION = "extension"
        }
        override fun onCreate(db: SQLiteDatabase) {
            val sql = "CREATE TABLE " + BASE + " (" + NAME + " TEXT," + ORIGINAL_NAME + " TEXT," + YEAR + " INTEGER,"+ ID + " INTEGER PRIMARY KEY,"+ PHOTO + " TEXT," + EXTENSION + " INTEGER" + ")"
            val CREATE_PRODUCTS_TABLE =
                ( "$sql" )
            db.execSQL(CREATE_PRODUCTS_TABLE)
        }
        override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
            db.execSQL("DROP TABLE IF EXISTS " + BASE)
            onCreate(db)
        }
        fun clear() {
            val db = this.writableDatabase
            db.execSQL("DELETE FROM " + BASE);
        }
        fun addProduct(item: Item) {
            val values = ContentValues().apply {
                put(NAME, item.name)
                put(ORIGINAL_NAME, item.original_name)
                put(YEAR, item.year)
                put(ID, item.id)
                put(PHOTO, item.photo)
                put(EXTENSION, item.extension)
            }
            if (this.findProduct(item.id)?.id == item.id) {
                Log.d("Found not unique item (id): ", item.id.toString())
                return
            }
            val db = this.writableDatabase
            db.insert(BASE, null, values)
            db.close()
        }
        fun findProduct(itemID: Long): Item? {
            val query = "SELECT * FROM $BASE WHERE $ID LIKE \"$itemID\""
            val db = this.readableDatabase
            val cursor = db.rawQuery(query, null)
            var item: Item? = null
            if (cursor.moveToFirst()) {
                val year = Integer.parseInt(cursor.getString(2))
                val id = Integer.parseInt(cursor.getString(3)).toLong()
                val name = cursor.getString(0)
                val original_name = cursor.getString(1)
                val photo = cursor.getString(4)
                val extension = Integer.parseInt(cursor.getString(5))
                item = Item(name, original_name, year, id, photo, extension)
                cursor.close()
            }
            db.close()
            return item
        }
        fun deleteProduct(itemID: Long): Boolean {
            var result = false
            val query = "SELECT * FROM $BASE WHERE $ID LIKE \"$itemID\""
            val db = this.writableDatabase
            val cursor = db.rawQuery(query, null)
            if (cursor.moveToFirst()) {
                val id = cursor.getLong(0)
                db.delete(BASE, ID + " = ?", arrayOf(id.toString()))
                cursor.close()
                result = true
            }
            db.close()
            return result
        }
        @SuppressLint("Range")
        fun printDBHandler(textView: TextView, context: Context) {
            var tableString = java.lang.String.format("Table %s:\n", BASE)
            val db = this.readableDatabase
            val allRows: Cursor = db.rawQuery("SELECT * FROM " + BASE, null)
            if (allRows.moveToFirst()) {
                val columnNames: Array<String> = allRows.getColumnNames()
                do {
                    Log.d("Log: ",allRows.position.toString())
                    for (name in columnNames) {
                        //if (name != NAME && name != ORIGINAL_NAME && name != YEAR && name != PHOTO && name != ID) break
                        tableString += java.lang.String.format(
                            "%s: %s\n", name, allRows.getString(allRows.getColumnIndex(name))
                        )
                    }
                    //Log.d("Log: ", "After ${allRows.position}: $tableString")
                    tableString += "\n\n"
                } while (allRows.moveToNext())
            }
            //Log.d("Log: ", tableString.toString())
            db.close()
            Toast.makeText(context, "Printing!", Toast.LENGTH_SHORT).show()
        }
        fun number_of_games(textView: TextView, username: String, extension: Int){
            val db = this.readableDatabase
            val cursor = db.rawQuery("SELECT * FROM " + BASE + " WHERE " + EXTENSION + " = $extension ", null)
            val count = cursor.count
            cursor.close()
            var end_str = ""
            if (extension == 1) end_str = username + "\n Number of expansions: " + count
            else end_str = username + "\n Number of non-expansions: " + count
            textView.text = end_str
        }
    }
    fun add_new_lines(textView: TextView) : TextView{
        var str = textView.text.toString()
        str = str.replace(' ','\n')
        textView.text = str
        return textView
    }
    @SuppressLint("Range")
    fun make_table(table: TableLayout, context: Context, database: MyDBHandler, extension: Int, username: String, userlistURL: String, multipleUser: String){
        val db = database.readableDatabase
        Log.d("Log: ", "Starts creating table")
        val cursor = db.rawQuery("SELECT * FROM " + MyDBHandler.BASE + " WHERE " + MyDBHandler.EXTENSION + " = $extension", null)
        val count = cursor.count
        cursor.close()
        //table.removeAllViews()
        Log.d("Log: ", "Number of rows: $count")
        val allRows: Cursor = db.rawQuery("SELECT * FROM " + MyDBHandler.BASE + " WHERE " + MyDBHandler.EXTENSION + " = $extension", null)
        if (count > 0) allRows.moveToFirst()
        for (i in 0..count) {
            val row = TableRow(context)
            row.setBackgroundColor(Color.LTGRAY)
            row.setPadding(15,15,15,15)
            //row.setPadding(0,0,0,2)
            val lp: TableRow.LayoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT)
            lp.setMargins(0,0,2,0)
            row.setLayoutParams(lp)
            val number = TextView(context)
            number.text = i.toString()
            val id = TextView(context)
            var name_ = TextView(context)
            val year = TextView(context)
            number.setPadding(50, 15, 50, 15)
            id.setPadding(50, 15, 50, 15)
            name_.setPadding(50, 15, 50, 15)
            year.setPadding(50, 15, 50, 15)
            number.setTextColor(context.resources.getColor(R.color.purple_500))
            id.setTextColor(context.resources.getColor(R.color.purple_500))
            name_.setTextColor(context.resources.getColor(R.color.purple_500))
            year.setTextColor(context.resources.getColor(R.color.purple_500))
            row.addView(number)
            val columnNames: Array<String> = allRows.getColumnNames()
            for (name in columnNames) {
                if (name == MyDBHandler.PHOTO){
                    val im = ImageView(context)
                    val executor = Executors.newSingleThreadExecutor()
                    val handler = Handler(Looper.getMainLooper())
                    var image: Bitmap? = null
                    executor.execute {
                        val imageURL = allRows.getString(allRows.getColumnIndex(name))
                        try {
                            val `in` = java.net.URL(imageURL).openStream()
                            image = BitmapFactory.decodeStream(`in`)
                            handler.post {
                                im.setImageBitmap(image)
                            }
                        } catch (e: Exception) {
                            image = BitmapFactory.decodeResource(
                                context.resources,
                                R.drawable.ic_launcher_background
                            )
                            im.setImageBitmap(image)
                            im.setColorFilter(R.color.purple_500)
                            e.printStackTrace()
                        }
                    }
                    row.addView(im)
                }
                if (name == MyDBHandler.ID) id.text = allRows.getString(allRows.getColumnIndex(name))
                if (name == MyDBHandler.NAME) name_.text = allRows.getString(allRows.getColumnIndex(name))
                if (name == MyDBHandler.YEAR) year.text = allRows.getString(allRows.getColumnIndex(name))
            }
            row.addView(id)
            name_ = add_new_lines(name_)
            row.addView(name_)
            row.addView(year)
            row.id = i
            row.setOnClickListener {
                game_view(row.id, id.text.toString(), context, database, username, userlistURL, multipleUser)
            }
            table.addView(row, i)
            if (count-1 == allRows.position) break
            if (count > allRows.position) allRows.moveToNext()
        }
        db.close()
    }
    fun clear_table(table: TableLayout){
        if (table.isEmpty()) return
        table.removeAllViews()
    }
    fun game_view(i: Int, id: String, context: Context, database: MyDBHandler, username: String, userlistURL: String, multipleUser: String){
        val intent = Intent(context, GamePanel::class.java)
        intent.putExtra("ItemID", database.findProduct(id.toLong())?.id.toString())
        intent.putExtra("Username", username)
        intent.putExtra("UserlistURL", userlistURL)
        intent.putExtra("MultipleUser", multipleUser)
        Log.d("Searched game id: ", database.findProduct(id.toLong())?.id.toString())
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_panel)
        val table = findViewById<TableLayout>(R.id.table)
        table.setBackgroundColor(Color.BLACK)
        table.setPadding(10,10,10,10)
        var global_dbHandler: MyDBHandler = MyDBHandler(this,null)
        global_dbHandler.clear()
        val username = intent.getStringExtra("Username").toString()
        var userlistURL = intent.getStringExtra("UserlistURL").toString()
        val multipleUser = intent.getStringExtra("MultipleUser").toString()
        if (multipleUser == "multiple") {
            global_dbHandler.clear()
            File("$filesDir/XML/").walk().forEach {
                Log.d("Deleted file: ", it.toString())
                it.delete()
            }
            Toast.makeText(this, "Database cleared successfully! \n This thread will colapse automaticaly.", Toast.LENGTH_SHORT).show()
            Thread.sleep(2000)
            clear_table(table)
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        userlistURL += "&subtype=boardgame"
        val infoText = findViewById<TextView>(R.id.info)
        infoText.text = username + "\n Number of games in database:\n(click on games buttons)"
        val extensionButton = findViewById<Button>(R.id.extensionButton)
        val notextensionButton = findViewById<Button>(R.id.notextensionButton)
        val synchButton = findViewById<Button>(R.id.synchButton)
        val deleteUserDataButton = findViewById<Button>(R.id.deleteUserInfoButton)
        var extension: Int = 0
        fun load(extenstion: Int, current: String) {
            var filename = ""
            if (extension==1) filename = "${username}_${current}_info_ext.xml"
            else filename = "${username}_${current}_info_pod.xml"
            val path = filesDir
            val inDir = File(path, "XML")

            if (inDir.exists()) {
                val file = File(inDir, filename)
                if (file.exists()) {
                    val xmlDoc: Document =
                        DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file)

                    xmlDoc.documentElement.normalize()

                    val items: NodeList = xmlDoc.getElementsByTagName("item")
                    //global_dbHandler.clear()
                    for (i in 0..items.length - 1) {
                        val itemNode: Node = items.item(i)
                        if (itemNode.nodeType == Node.ELEMENT_NODE) {
                            val elem = itemNode as Element
                            val children = elem.childNodes
                            var id: String? = null
                            var title: String? = null
                            var org_title: String? = null
                            var year_pub: String? = null
                            var rank_pos: String? = null
                            var pic: String? = null
                            var tmp: String? = null
                            val tags = itemNode.attributes
                            for (j in 0..tags.length - 1) {
                                val node = tags.item(j)
                                when (node.nodeName) {
                                    "objectid" -> {
                                        id = node.nodeValue
                                    }
                                }
                            }
                            for (j in 0..children.length - 1) {
                                val node = children.item(j)
                                if (node is Element) {
                                    when (node.nodeName) {
                                        "name" -> {
                                            title = node.textContent
                                        }
                                        "yearpublished" -> {
                                            year_pub = node.textContent
                                        }
                                        "thumbnail" -> {
                                            pic = node.textContent
                                        }
                                        "stats" -> {
                                            val n = node.childNodes
                                            for (j1 in 0..n.length - 1) {
                                                val node = n.item(j1)
                                                if (node is Element) {
                                                    when (node.nodeName) {
                                                        "rating" -> {
                                                            val n = node.childNodes
                                                            for (j2 in 0..n.length - 1) {
                                                                val node = n.item(j2)
                                                                if (node is Element) {
                                                                    when (node.nodeName) {
                                                                        "ranks" -> {
                                                                            val n = node.childNodes
                                                                            for (j3 in 0..n.length - 1) {
                                                                                val node = n.item(j3)
                                                                                if (node is Element) {
                                                                                    val tags =
                                                                                        node.attributes
                                                                                    for (j4 in 0..tags.length - 1) {
                                                                                        val node =
                                                                                            tags.item(j4)
                                                                                        when (node.nodeName) {
                                                                                            "id" -> {
                                                                                                tmp =
                                                                                                    node.nodeValue
                                                                                            }
                                                                                            "value" -> {
                                                                                                rank_pos =
                                                                                                    node.nodeValue
                                                                                            }
                                                                                        }
                                                                                        if (tmp == "1" && rank_pos != null) {
                                                                                            break
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            var _extension: Int = extension
                            //if(small){
                            //    _extension = 1
                            //}
                            if (rank_pos == "Not Ranked" || rank_pos == null) {
                                rank_pos = "0"
                            }
                            var _id: Long = Integer.parseInt(id).toLong()
                            var _title: String = title.toString()
                            var _org_title: String = org_title.toString()
                            var _year_pub: Int = 0
                            if (year_pub == null) {
                                _year_pub = 1900
                            } else {
                                _year_pub = Integer.parseInt(year_pub)
                            }
                            var _rank_pos: Int = Integer.parseInt(rank_pos)
                            var _pic: String? = pic

                            val item = Item(_title, _org_title, _year_pub, _id, _pic, _extension)
                            global_dbHandler.addProduct(item)
                            //Log.d("Log: ","New game: ${item.id}")
                            if (_rank_pos != 0) {
                                //val s = Stat(_id, _rank_pos, lastSync)
                                //dbStat.addStat(s)
                            }
                        }
                    }
                    //global_dbHandler.printDBHandler(infoText, this)
                    clear_table(table)
                    make_table(table, this, global_dbHandler, extension, username, userlistURL, multipleUser)
                    global_dbHandler.number_of_games(infoText, username, extension)
                }
            }
        }
        fun download(url: String, extension: Int, current: String) {
            val urlString = url
            val xmlDirectory = File("$filesDir/XML")
            if (!xmlDirectory.exists()) xmlDirectory.mkdir()
            val fileName : String
            if (extension==1) fileName = "$xmlDirectory/" + username + "_" + current + "_info_ext.xml"
            else fileName = "$xmlDirectory/" + username + "_" + current + "_info_pod.xml"
            CoroutineScope(Dispatchers.IO).launch {
                try {
                    val ur = URL(urlString)
                    val reader = ur.openStream().bufferedReader()
                    if (File(fileName).exists()) deleteFile(fileName)
                    val downloadFile = File(fileName).also { it.createNewFile() }
                    if (downloadFile.createNewFile()) Log.d("Create new file: ", fileName)
                    else Log.d("Creating file failed: ", fileName)
                    if (File(fileName).exists()) Log.d("File exists:", fileName)
                    val writer = FileWriter(downloadFile).buffered()
                    var line: String
                    while (reader.readLine().also { line = it?.toString() ?: "" } != null)
                        writer.write(line)
                    reader.close()
                    writer.close()

                    withContext(Dispatchers.Main) {
                        load(extension, current)
                        //showData()
                    }
                } catch (e: Exception) {
                    withContext(Dispatchers.Main) {
                        when (e) {
                            is MalformedURLException ->
                                print("Malformed URL")
                            else ->
                                print("Error")
                        }
                        val incompleteFile = File(fileName)
                        if (incompleteFile.exists()) incompleteFile.delete()
                    }
                }
            }
        }
        extensionButton.setOnClickListener{
            extension = 1
            infoText.text = "Please wait..\nDownloading your games!"
            val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")
            val current = LocalDateTime.now().format(formatter)
            CoroutineScope(Dispatchers.IO).launch {
                download(userlistURL+"expansion", extension, current)
                //Thread.sleep(2500)
                //download(userlistURL+"expansion", extension)
            }
        }
        notextensionButton.setOnClickListener{
            extension = 0
            infoText.text = "Please wait..\nDownloading your games!"
            val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")
            val current = LocalDateTime.now().format(formatter)
            CoroutineScope(Dispatchers.IO).launch {
                download(userlistURL+"&excludesubtype=boardgameexpansion", extension, current)
                //Thread.sleep(2500)
                //download(userlistURL+"&excludesubtype=boardgameexpansion", extension)
            }
        }
        deleteUserDataButton.setOnClickListener{
            global_dbHandler.clear()
            File("$filesDir/XML/").walk().forEach {
                Log.d("Deleted file: ", it.toString())
                it.delete()
            }
            Toast.makeText(this, "Database cleared successfully! \n This thread will colapse automaticaly.", Toast.LENGTH_SHORT).show()
            Thread.sleep(2000)
            clear_table(table)
            exitProcess(1)
        }
        synchButton.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}