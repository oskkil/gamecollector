package edu.put.inf151863

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.ListAdapter
import android.widget.ListView
import android.widget.SimpleAdapter
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import kotlinx.coroutines.*
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import org.xmlpull.v1.XmlPullParserFactory
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.net.MalformedURLException
import java.net.URL
import javax.xml.parsers.DocumentBuilderFactory

class GamePanel : AppCompatActivity() {
    fun download(url: String, id: Long, textView: TextView) {
        val urlString = url
        val xmlDirectory = File("$filesDir/XML")
        if (!xmlDirectory.exists()) xmlDirectory.mkdir()
        val fileName = "$xmlDirectory/game_$id.xml"
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val ur = URL(urlString)
                val reader = ur.openStream().bufferedReader()
                val downloadFile = File(fileName).also { it.createNewFile() }
                val writer = FileWriter(downloadFile).buffered()
                var line: String
                while (reader.readLine().also { line = it?.toString() ?: "" } != null)
                    writer.write(line)
                reader.close()
                writer.close()

                withContext(Dispatchers.Main) {
                    //showData()
                    load(id, textView)
                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    when (e) {
                        is MalformedURLException ->
                            print("Malformed URL")
                        else ->
                            print("Error")
                    }
                    val incompleteFile = File(fileName)
                    if (incompleteFile.exists()) incompleteFile.delete()
                }
            }
        }
    }
    fun load(id: Long, textView: TextView) {
        val filename = "game_$id.xml"
        val path = filesDir
        val inDir = File(path, "XML")

        if (inDir.exists()) {
            val file = File(inDir, filename)
            if (file.exists()) {
                val xmlDoc: Document =
                    DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file)

                xmlDoc.documentElement.normalize()

                val items: NodeList = xmlDoc.getElementsByTagName("item")
                for (i in 0..items.length - 1) {
                    val itemNode: Node = items.item(i)
                    if (itemNode.nodeType == Node.ELEMENT_NODE) {
                        val elem = itemNode as Element
                        val children = elem.childNodes
                        var id: Long? = null
                        var primary: String? = null
                        var description: String? = null
                        var minplayers: String? = null
                        var maxplayers: String? = null
                        var year: String? = null
                        var flag = false
                        val tags = itemNode.attributes
                        for (j in 0..tags.length - 1) {
                            val node = tags.item(j)
                            //Log.d("Log: ", node.toString())
                            when (node.nodeName) {
                                "id" -> {
                                    id = node.nodeValue.toString().toLong()
                                }
                            }
                        }
                        for (i in 0..children.length - 1) {
                            val node = children.item(i)
                            if (node is Element) {
                                //Log.d("Log: ", node.nodeName)
                                when (node.nodeName) {
                                    "name" -> {
                                        val tags2 = node.attributes
                                        for (j in 0..tags2.length - 1) {
                                            val node2 = tags2.item(j)
                                            //Log.d("Log: ", node2.nodeName)
                                            when (node2.nodeName) {
                                                "type" -> {
                                                    if (node2.textContent == "primary") {
                                                        //Log.d("Found name: ", node2.textContent)
                                                        flag = true
                                                        primary = node2.textContent
                                                    }
                                                }
                                                "value" -> {
                                                    if (flag) {
                                                        //Log.d("That name is: ", node2.textContent)
                                                        primary = node2.textContent
                                                        flag = false
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    "description" -> {
                                        description = node.textContent
                                    }
                                    "minplayers" -> {
                                        val tags2 = node.attributes
                                        for (j in 0..tags2.length - 1) {
                                            val node2 = tags2.item(j)
                                            //Log.d("Log: ", node2.nodeName)
                                            when (node2.nodeName) {
                                                "value" -> {
                                                    minplayers = node2.textContent
                                                }
                                            }
                                        }
                                    }
                                    "maxplayers" -> {
                                        val tags2 = node.attributes
                                        for (j in 0..tags2.length - 1) {
                                            val node2 = tags2.item(j)
                                            //Log.d("Log: ", node2.nodeName)
                                            when (node2.nodeName) {
                                                "value" -> {
                                                    maxplayers = node2.textContent
                                                }
                                            }
                                        }
                                    }
                                    "yearpublished" -> {
                                        val tags2 = node.attributes
                                        for (j in 0..tags2.length - 1) {
                                            val node2 = tags2.item(j)
                                            //Log.d("Log: ", node2.nodeName)
                                            when (node2.nodeName) {
                                                "value" -> {
                                                    year = node2.textContent
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        textView.text = "Id: $id \n " +
                                "Primary: $primary \n " +
                                "Description: $description \n " +
                                "Minimal number of players: $minplayers \n " +
                                "Maximal number of players: $maxplayers \n" +
                                "Publish year: $year \n"
                    }
                }
            }
        }
    }
    object BitmapScaler {
        fun scaleToFitWidth(b: Bitmap, width: Int): Bitmap {
            val factor = width / b.width.toFloat()
            return Bitmap.createScaledBitmap(b, width, (b.height*factor).toInt(), true)
        }
        fun scaleToFitHeight(b: Bitmap, height: Int): Bitmap {
            val factor = height / b.height.toFloat()
            return Bitmap.createScaledBitmap(b, (b.width*factor).toInt(), height, true)
        }
    }
    private fun getCapturedImage(selectedPhotoUri: Uri): Bitmap {
        val bitmap = when {
            Build.VERSION.SDK_INT < 28 -> MediaStore.Images.Media.getBitmap(
                this.contentResolver,
                selectedPhotoUri
            )
            else -> {
                val source = ImageDecoder.createSource(this.contentResolver, selectedPhotoUri)
                ImageDecoder.decodeBitmap(source)
            }
        }
        return BitmapScaler.scaleToFitWidth(bitmap, 50)
    }
    lateinit var imageView : ImageView
    var mGetContent = registerForActivityResult(
        ActivityResultContracts.GetContent()
    ) { result ->
        if (result != null) {
            imageView.setImageURI(result)
            imageView.setImageBitmap(getCapturedImage(result))
        }
    }
    private fun initTempUri(): Uri {
        val tempImagesDir = File(
            applicationContext.filesDir,
            getString(R.string.temp_images_dir))
        tempImagesDir.mkdir()
        val tempImage = File(
            tempImagesDir,
            getString(R.string.temp_image))
        return FileProvider.getUriForFile(
            applicationContext,
            getString(R.string.authorities),
            tempImage)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_panel)
        val id = intent.getStringExtra("ItemID").toString().toLong()
        val url = "https://www.boardgamegeek.com/xmlapi2/thing?id=$id"
        val textView = findViewById<TextView>(R.id.textView)
        val imageView = findViewById<ImageView>(R.id.image)
        textView.movementMethod = ScrollingMovementMethod()
        val addImageButton = findViewById<Button>(R.id.addImageButton)
        val galleryButton = findViewById<Button>(R.id.galleryButton)
        val returnButton = findViewById<Button>(R.id.returnButton)
        val username = intent.getStringExtra("Username").toString()
        var userlistURL = intent.getStringExtra("UserlistURL").toString()
        val multipleUser = intent.getStringExtra("MultipleUser").toString()
        val tempImageUri = initTempUri()
        val resultLauncher =
            registerForActivityResult(ActivityResultContracts.TakePicture()) {
                imageView.setImageURI(null)
                imageView.setImageURI(tempImageUri)
            }
        addImageButton.setOnClickListener {
            resultLauncher.launch(tempImageUri)
        }
        galleryButton.setOnClickListener{
            mGetContent.launch(
                "image/*"
            )
        }
        returnButton.setOnClickListener{
            val intent = Intent(this, UserPanel::class.java)
            intent.putExtra("Username", username)
            intent.putExtra("UserlistURL", userlistURL)
            intent.putExtra("MultipleUser", multipleUser)
            startActivity(intent)
        }
        CoroutineScope(Dispatchers.IO).launch {
            download(url, id, textView)
            //Thread.sleep(1000)
            //download(url, id)
        }
    }
}
